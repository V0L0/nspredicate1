//
//  ViewController.swift
//  NSPredicate#1
//
//  Created by Volodymyr on 18.01.2020.
//  Copyright © 2020 Volodymyr. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        call()
        
    }
    func call() -> String {
        let resalt = ""
        let dataSource = [
            "Domain CheckService",
            "IMEI check",
            "Compliant about service provider",
            "Compliant about TRA",
            "Enquires",
            "Suggestion",
            "SMS Spam",
            "Poor Coverage",
            "Help Salim"
        ]
        
        let searchString = "z"
        
        let predicate = NSPredicate(format: "SELF contains %@", searchString)
        print(predicate)
        let searchDataSource = dataSource.filter { predicate.evaluate(with: $0) }
        print("\n")
        print(searchDataSource)
        return resalt
    }
    
    
}
